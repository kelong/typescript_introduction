function displayTotal(purchase, taxCalculator) {
    console.log("Total value is " + (purchase + taxCalculator.getTax(purchase)).toString());
}
var DefaultTaxCalculator = (function () {
    function DefaultTaxCalculator() { }
    DefaultTaxCalculator.prototype.getTax = function (purchase) {
        return purchase * 0.1;
    };
    return DefaultTaxCalculator;
})();
var taxCalculator = new DefaultTaxCalculator();
displayTotal(100, taxCalculator);
displayTotal(100, {
    getTax: function (purchase) {
        return purchase * 0.15;
    }
});
