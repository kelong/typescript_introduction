/// <reference path="ambients.d.ts"/>
var p = new Person("Daisy", "Duck");

p.displayName();
p.displayFirstName();

console.log(p.first);

// access named instance - mickey
mickey.displayName();
mickey.first;

// access named instance - donald
donald.displayFirstName();
