function addTwoNumbers(number1:number, number2:number):number {
    console.log(number1 + number2);
    return number1 + number2;
}

addTwoNumbers(10, 20);

// this will not work
// var name:string = addTwoNumbers(10, 20);

// alternate way to define the same function

var add = (n1: number, n2: number) :number => {
    console.log(n1 + n2);
    return n1 + n2;
}

add(10, 20);


// it is also possible to define a function's type and then assign it to an instance of a function
// that meets type specifications.

var addAndLogFunction: (n1: number, n2: number) => number;

addAndLogFunction = addTwoNumbers;
addAndLogFunction(10, 20);
addAndLogFunction = add;
addAndLogFunction(10, 20);


