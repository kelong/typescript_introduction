// interface definition
interface ITaxcalculator {
    getTax(purchase: number): number;
}

// function that consumes interface
function displayTotal(purchase: number, taxCalculator: ITaxcalculator) {
    console.log("Total value is " + (purchase + taxCalculator.getTax(purchase)).toString());
}

// class that implements our interface
class DefaultTaxCalculator implements ITaxcalculator {

    getTax(purchase: number): number {
        return purchase * 0.1;
    }
}

// this works
var taxCalculator = new DefaultTaxCalculator();
displayTotal(100, taxCalculator);

// interface implemented as an object literal
// this also works
displayTotal(100, {
    getTax: function (purchase: number) {
        return purchase * 0.15;
    }
});

// uncomment the line below to see the compiler catch an incorrect object being passed in
// this object does not implement the required interface
//displayTotal(100, {});