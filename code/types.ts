function addTwoNumbers(number1, number2) {
    console.log(number1 + number2);
}

// This is correct
addTwoNumbers(10, 20);

// This is not correct but JavaScript will not mind
addTwoNumbers("10", 20);


function addTwoNumbersTS(number1:number, number2:number) {
    console.log(number1 + number2);
}

// This works
addTwoNumbersTS(10, 20);

// This is not correct and TypeScript will flag this
// addTwoNumbersTS("10", 20);


