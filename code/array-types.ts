function displayNames(names: string[]) {
    for (var i = 0, length = names.length; i < length; i++)
        console.log(names[i].toUpperCase());
}

var names = ['albert', 'ALFRED', 'Donald', 'Jack', 'Clarabel', 'Fran'];
displayNames(names);

// the following code will cause a compiler error
// displayNames([1,2,3]);