// This is regular JavaScript code
// There is no typescipt in this file

// Person constructor and related functions
function Person(first, last) {
    this.first = first;
    this.last = last;
}
Person.prototype.displayName = function () {
    console.log(this.last + "," + this.first);
};
Person.prototype.displayFirstName = function () {
    console.log(this.first);
};

// named instance of person
var mickey = new Person("Mickey", "Mouse");

// named instance of another person
var donald = new Person("Mickey", "Mouse");